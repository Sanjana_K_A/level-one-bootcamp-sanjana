//WAP to find the distance between two point using 4 functions.
//Program to find distance between two points using 4 functions
#include <stdio.h>
#include <math.h>
int input();
float compute(int, int);
void display(float);
int main()
{
int x, y;
float z;
printf("Enter x coordinates of point 1 and point 2\n");
x = input();
printf("Enter y coordinates of point 1 and point 2\n");
y = input();
z = compute(x, y);
display(z);
return 0;
}
int input()
{
int n2, n1, n;
scanf("%d %d",&n1, &n2);
n = n2 - n1;
return n;
}
float compute(int p, int q)
{
float distance;
distance = sqrt(pow(p,2) + pow(q,2));
return distance;
}
void display(float result)
{
printf("The distance between two points is: %f",result);
}