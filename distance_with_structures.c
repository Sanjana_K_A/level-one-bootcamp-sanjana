//WAP to find the distance between two points using structures and functions.
#include <stdio.h>
#include <math.h>
struct input_points
{
    float x;
    float y;
};
typedef struct input_points Point;
Point input()
{
    Point p;
    scanf("%f %f",&p.x,&p.y);
    return p;
}
float compute(Point p1,Point p2)
{
    float distance;
    float m,n;
    m = p2.x - p1.x;
    n = p2.y - p1.y;
    distance=sqrt((m*m)+(n*n));
    return distance;
}
void output(float result)
{
    printf("The distance between the two points is %f",result);
}
int main()
{
    float result;
    Point p1,p2;
    printf("Enter the x-coordinate and y-coordinate of point 1: ");
    p1=input();
    printf("Enter the x-coordinate and y-coordinate of point 2: ");
    p2=input();
    result=compute(p1,p2);
    output(result);
    return 0;
}
