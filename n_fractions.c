//WAP to find the sum of n fractions.
#include<stdio.h>
#include<math.h>
struct fraction
{
    int n, d;
}p1, p2, total;

typedef struct fraction Fraction;

int add(int m, int n1, int d1, int n2, int d2)
{
    int sum = (m/d1)*n1+(m/d2)*n2;
    return sum;
}
int lcm(Fraction p1, Fraction p2)
{ 
    int lcm;
    for(lcm=1;lcm<=p1.d*p2.d;lcm++)
    {
        if(lcm%p1.d == 0 && lcm%p2.d == 0)
            break;
    }
   return lcm;
} 

int main()
{
    int k, m, n, res, res1;
    printf("Enter total number of fractions to be added: \n");
    scanf("%d",&k);
    printf("Enter the numerator and denominator of the fraction: \n") ;
    scanf("%d %d",&p1.n,&p1.d);

for(int i=0;i<k-1;i++)
{
    printf("Enter the numerator and denominator of the fraction: \n");
    scanf("%d %d",&p2.n,&p2.d);

    n = lcm(p1,p2);
    m = add(n,p1.n,p1.d,p2.n,p2.d);
    total.n = m;
    total.d = n;
    p1.n = total.n;
    p1.d = total.d;
}
    printf("The sum of fractions is %d / %d\n",total.n, total.d);
    return 0;
}