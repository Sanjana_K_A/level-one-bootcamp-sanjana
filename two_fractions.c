//WAP to find the sum of two fractions.
//Program to add 2 fractions using structures and functions
#include <stdio.h>
struct input_num
{
    int n;
    int d;
};
typedef struct input_num Number;
Number input()
{
    Number p;
    scanf("%d %d",&p.n,&p.d);
    return p;
}
void compute(Number p1,Number p2)
{
    int sum;
    int m,n;
    m = (p1.n*p2.d) + (p1.d*p2.n);
    n = p1.d * p2.d;
    printf("The sum of two fractions is %d/%d",m,n);
}
int main()
{
    int result;
    Number p1,p2;
    printf("Enter the numerator and denominator of 1st fraction: ");
    p1=input();
    printf("Enter the numerator and denominator of 2nd fraction: ");
    p2=input();
    compute(p1,p2);
    
    return 0;
}