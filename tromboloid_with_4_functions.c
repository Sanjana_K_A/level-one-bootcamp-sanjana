//WAP to find the volume of a tromboloid using 4 functions.
//Program to add two numbers using 4 functions
#include <stdio.h>
float number();
float compute(float, float, float);
void display(float);
int main()
{
float w, x, y, z;
printf("Enter the height of the tromboloid: ");
w = number();
printf("Enter the depth of the tromboloid: ");
x = number();
printf("Enter the breadth of the tromboloid: ");
y = number();
z = compute(w,x,y);
display(z);
return 0;
}
float number()
{
float num;
scanf("%f",&num);
return num;
}
float compute(float h,float d, float b)
{
float result;
result = ((h*d*b)+d/b)/3;
return result;
}
void display(float volume)
{
printf("Volume of tromboloid = %f",volume);
}
